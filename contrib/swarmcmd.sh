#!/bin/bash

if [ "$*" == "" ]; then
  echo "Need something to do"
  exit -1
fi

HOST="192.168.5.54"

for x in $( docker -H ${HOST} node ls | awk '{print $1}'); do
  if [ "$x" == "ID" ]; then continue; fi
  NAME=$( docker -H ${HOST} node inspect -f '{{ .Description.Hostname }}' $x )
  IPADDR=$( docker -H ${HOST} node inspect -f '{{ .Status.Addr }}' $x )
  echo "----------------------------------------------------------------------"
  echo "${NAME} [${IPADDR}]"
  echo "----------------------------------------------------------------------"
  ssh -q -o ConnectTimeout=10 -o UserKnownHostsFile=/dev/null -o GSSAPIKeyExchange=no -o StrictHostKeyChecking=no -i ~/.ssh/kooper.pem core@$IPADDR $*
done
