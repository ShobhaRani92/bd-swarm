import docker
import docker.types
import docker.errors
import os.path
import urllib.parse

tag = 'latest'

client = docker.DockerClient(base_url='tcp://SWARM_MASTER_IP:SWARM_MASTER_PORT', version='auto')

rabbitmq = {
    "prod-extractor": "amqp://USERNAME:PASSWORD@RABBITMQ/clowder",
    "dev-extractor": "amqp://USERNAME:PASSWORD@RABBITMQ/clowder-dev",
    "prod-converter": "amqp://USERNAME:PASSWORD@RABBITMQ/dap-prod",
    "dev-converter": "amqp://USERNAME:PASSWORD@RABBITMQ/dap-dev",
}
geo = [
    "GEOSERVER_URL=https://GEOSERVER/geoserver/",
    "GEOSERVER_PASSWORD=GEOSERVER_ADMIN_PASSWORD",
    "GEOSERVER_WORKSPACE=clowder",
]

# converters
converters = {
    "avconv": "ncsapolyglot/converters-avconv",
    "Clowder": "ncsapolyglot/converters-clowder",
    "daffodil": "ncsapolyglot/converters-daffodil",
    "ImageMagick": "ncsapolyglot/converters-imagemagick",
    "ebook-convert": "ncsapolyglot/converters-ebook-convert",
    "flac": "ncsapolyglot/converters-flac",
    "GDAL": "ncsapolyglot/converters-gdal",
    "GeoExpress": "ncsapolyglot/converters-geoexpress",
    "ghostscript": "ncsapolyglot/converters-ghostscript",
    "kabeja": "ncsapolyglot/converters-kabeja",
    "OpenJPEG": "ncsapolyglot/converters-openjpeg",
    "ncdump": "ncsapolyglot/converters-ncdump",
    "unoconv": "ncsapolyglot/converters-unoconv",
    "zip": "ncsapolyglot/converters-zip",
    "txt2html": "ncsapolyglot/converters-txt2html"
}

# extractors
extractors = {
    "ncsa.cv.closeups": "clowder/extractors-opencv-closeups",
    "ncsa.cv.eyes": "clowder/extractors-opencv-eyes",
    "ncsa.cv.faces": "clowder/extractors-opencv-faces",
    "ncsa.cv.profiles": "clowder/extractors-opencv-profiles",
    "ncsa.image.ocr": "clowder/extractors-tesseract",
    "ncsa.chi.analysis": "clowder/extractors-chi-analysis",
    "ncsa.cv.river": "clowder/extractors-river",
    "ncsa.image.geotiff": "clowder/extractors-image-geotiff",
    "ncsa.dbpedia": "clowder/extractors-dbpedia",
    "ncsa.cv.meangrey": "clowder/extractors-meangrey",
    "ncsa.versus.image": "clowder/extractors-versus",
    "siegfried": "clowder/extractors-siegfried",
    "gi.detector": "clowder/extractors-gi-detector",
    "terra.plantcv": "clowder/extractors-plantcv",

    "ncsa.file.digest": "clowder/extractors-digest",
    "ncsa.audio.preview": "clowder/extractors-audio-preview",
    "ncsa.image.preview": "clowder/extractors-image-preview",
    "ncsa.image.metadata": "clowder/extractors-image-metadata",
    "ncsa.video.preview": "clowder/extractors-video-preview",
    "ncsa.pdf.preview": "clowder/extractors-pdf-preview",
    "ncsa.office.preview": "clowder/extractors-office-preview",

    "ncsa.geo.shp": "clowder/extractors-geo-shp",
    "ncsa.geo.tiff": "clowder/extractors-geo-tiff",
}


def create_service(dev_prod, service_type, queue, image, replicas=0, env=None):
    global rabbitmq, geo, client

    name = '%s-%s-%s' % (dev_prod, service_type, queue.replace(".", "_"))
    rabbitmq_url = rabbitmq.get("%s-%s" % (dev_prod, service_type), None)
    vhost = os.path.basename(urllib.parse.urlparse(rabbitmq_url).path)

    try:
        service = client.services.get(name)
    except docker.errors.NotFound:
        service = None

    if service:
        x = service.attrs['Spec']['Mode']['Replicated']['Replicas']
        if x != replicas:
            replicas = x
        replicas = 2

    if env is None:
        env = list()
    env.append("RABBITMQ_URI=%s" % rabbitmq_url)
    if service_type == "extractor" and "geo" in queue:
        env.extend(geo)

    labels = dict()
    labels['bd.level'] = dev_prod
    labels['bd.type'] = service_type
    labels['bd.replicas.min'] = "1"
    labels['bd.replicas.max'] = "5"
    labels['bd.rabbitmq.queue'] = queue
    labels['bd.rabbitmq.vhost'] = vhost

    mode = docker.types.ServiceMode('replicated', replicas)
    if replicas == 0:
        mode.get('replicated')['Replicas'] = 0

    if service:
        service.update(image=image,
                       name=name,
                       env=env,
                       mode=mode,
                       labels=labels,
                       resources=docker.types.Resources(cpu_limit=1000000000))
    else:
        client.services.create(image=image,
                               name=name,
                               env=env,
                               mode=mode,
                               labels=labels,
                               resources=docker.types.Resources(cpu_limit=1000000000))

for prefix in ["prod", "dev"]:
    for queue, image in extractors.items():
        create_service(prefix, 'extractor', queue, '%s:%s' % (image, tag))

    for queue, image in converters.items():
        create_service(prefix, 'converter', queue, '%s:%s' % (image, tag))

env = list()
env.append("THUMBNAIL_FLAGS=-set colorspace RGB -resize 225^ -brightness-contrast 2x6% -colorspace sRGB")
env.append("PREVIEW_FLAGS=-set colorspace RGB -resize 800x600 -brightness-contrast 2x6% -colorspace sRGB")
env.append("RABBITMQ_QUEUE=ncsa.image.preview.debod")
create_service('prod', 'extractor', 'ncsa.image.preview.debod', 'clowder/extractors-image-preview:%s' % tag, env=env)
