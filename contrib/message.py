import json
import re
import os.path
import urllib
import urlparse

import pika

rabbitmq_uri = "amqp://username:password@rabbitmq.ncsa.illinois.edu/"
vhost = "dap-dev"
queues = ["unoconv"]
drophost = ['http://host:9000']

# connect to rabbitmq
parameters = pika.URLParameters(rabbitmq_uri + vhost)
connection = pika.BlockingConnection(parameters)

# connect to channel
channel = connection.channel()

# setting prefetch count to 1 so we only take 1 message of the bus at a time,
# so other extractors of the same type can take the next message.
channel.basic_qos(prefetch_count=1)

for queue in queues:
    res = channel.queue_declare(queue=queue, durable=True, passive=True)
    messages = res.method.message_count

    # process messages
    print('Messages in queue %d' % messages)
    dropped = 0
    if messages > 0:
        for method, header, body in channel.consume(queue):
            jbody = json.loads(body)
            dropmessage = False

            if vhost.startswith("dap-"):
                # polyglot
                host = re.sub(r'(https?://)(?:[^@]*@)?([^/]*).*', r'\1\2', jbody.get('input', ''))
            else:
                # clowder
                host = jbody.get('host', '')

            # filename based filtering
            o = urlparse.urlparse(jbody.get('input', ''))
            filename = os.path.basename(o.path)
            if filename != urllib.quote_plus(filename):
                print("Message dropped, bad filename : %s != %s" % (filename, urllib.quote_plus(filename)))
                dropmessage = True
            # host based filtering
            if host in drophost:
                print("Message host in drophost list : %s" % host)
                dropmessage = True

            # check if need to drop message from host
            if not dropmessage:
                if 'exchange' not in jbody and method.exchange:
                    jbody['exchange'] = method.exchange
                if 'routing_key' not in jbody and method.routing_key and method.routing_key != queue:
                    jbody['routing_key'] = method.routing_key

                properties = pika.BasicProperties(delivery_mode=2, reply_to=header.reply_to)
                channel.basic_publish(exchange='',
                                      routing_key=queue,
                                      properties=properties,
                                      body=json.dumps(jbody))
            else:
                dropped += 1

            # done with message
            channel.basic_ack(method.delivery_tag)

            messages -= 1
            if messages <= 0:
                break

    print('Messages dropped %d' % dropped)
channel.close()
connection.close()
