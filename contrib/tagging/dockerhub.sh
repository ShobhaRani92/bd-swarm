#!/bin/bash

set -e

# DEBUG   : set to echo to print command and not execute
# PUSH    : set to push to push, then push newtag to docker hub

DEBUG=echo
PUSH=${PUSH:-"push"}

TAG=latest
# new tag to be pushed on docker hub
NewTag=browndog

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]
then
    echo "failed to give all input params"
    exit 128
fi

# set username and password
USER=$1
PASSWD=$2
REPONAME=$3

case "${REPONAME}" in
    ncsapolyglot*)     PREFIX="converters-";;
    clowder*)    PREFIX="extractors-";;
    *)        echo "UNKNOWN:${REPONAME}"; exit 128
esac

TOKEN=$(curl -s -H "Content-Type: application/json" -X POST -d '{"username": "'${USER}'", "password": "'${PASSWD}'"}' https://hub.docker.com/v2/users/login/ | jq -r .token)

REPO_LIST="${@:4}"
if [ -z "$REPO_LIST" ]
then
    REPO_LIST=$(curl -X GET -s -H "Authorization: JWT ${TOKEN}" https://hub.docker.com/v2/repositories/${REPONAME}/?page_size=10000 | jq --arg PREFIX $PREFIX -r '.results|.[] | select(.name | contains($PREFIX)) | .name')
fi

COUNTER=0
for name in ${REPO_LIST}
do
  Latest_Tag=$(curl -s -H "Authorization: JWT ${TOKEN}" https://hub.docker.com/v2/repositories/${REPONAME}/${name}/tags/?page_size=10000 | jq --arg TAG $TAG -r '.results|.[] | select(.name == $TAG) | .name')
  if [ -z $Latest_Tag ]; then
  	echo $name "does not have " $TAG
  	continue;
  fi
  ${DEBUG} docker pull ${REPONAME}/$name:${TAG}
  ${DEBUG} docker tag ${REPONAME}/$name:${TAG} ${REPONAME}/$name:$NewTag
  if [ "$PUSH" = "push" ]; then
    ${DEBUG} docker push ${REPONAME}/${name}:${NewTag}
    COUNTER=$((COUNTER+1))
  fi
done

echo -e "\n[Done] retag" $COUNTER $PREFIX" to docker account:"$REPONAME "with new tag:"$NewTag




