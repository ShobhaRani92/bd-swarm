Tag on repositories at https://opensource.ncsa.illinois.edu. 

Retag extractors:latest (docker image's name starting with extractors-) to extractors:browndog under clowder account on docker hub.

Retag converters:latest (docker image's name starting with converters-) to extractors:browndog under ncsapolyglot account on docker hub.

Machine needs to be installed with jq and docker. Before tagging, make sure do 'docker login' first and you have permission to do 'docker push'
on clowder account and ncsapolyglot account on docker hub.
### install jq
mac
```
brew install jq
```

linux
```
apt-get install jq
```

### Tag/unTag on Repositories

to tag converters on repositories of POL, need to enter your username/passwd of https://opensource.ncsa.illinois.edu
```
./repos.sh username passwd tag POL
```
to untag converters on repositories of POL
```
./repos.sh username passwd untag POL
```

to tag extractors on repositories of CATS
```
./repos.sh username passwd tag CATS
```
to untag extractors on repositories of CATS
```
./repos.sh username passwd untag CATS
```


### Tagging on Docker Hub
to tag converters on docker hub, need to enter your username/passwd of https://hub.docker.com/
```
./dockerhub.sh username passwd ncsapolyglot
```

to tag extractors on docker hub,
```
./dockerhub.sh username passwd clowder
```

Instead of tagging all extractors or converters on dockerhub, users can specify the name of converters or extractors to tag on dockerhub.
```
./dockerhub.sh username passwd ncsapolyglot converters-unoconv converters-avconv converters-imagemagick
```
