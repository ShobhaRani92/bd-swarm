#!/bin/bash

PREFIX=${1:-'.*'}

HOST="192.168.5.54"

for x in $(docker -H ${HOST} node ls | awk '{print $1}'); do
    if [ "$x" == "ID" ]; then continue; fi
    IPADDR=$(docker -H ${HOST} node inspect -f '{{ .Status.Addr }}' $x )

    for extractor in $(docker -H $IPADDR ps | awk '{print $NF}' | egrep -o "(($PREFIX\S+))"); do
    	if [ "$extractor" == "NAMES" ]; then continue; fi
        echo "check intermediate files: " $extractor
        docker -H $IPADDR diff $extractor
        echo "---------------------"
    done
done
