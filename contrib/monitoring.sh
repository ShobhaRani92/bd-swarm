#!/bin/bash

HOST=192.168.5.104
CLUSTER=192.168.5.54
RABBITMQ="{
  'dev-converter': 'amqp://username:password@rabbitmq.ncsa.illinois.edu:15672/dap-dev',
  'dev-extractor' : 'amqp://username:password@rabbitmq.ncsa.illinois.edu:15672/clowder-dev'
}"
PULL=${1:-""}

docker -H ${HOST} system prune -f

if [ "$(docker -H ${HOST} ps -q --filter 'name=portainer')" == "" ]; then
  docker -H ${HOST} run -d \
    --name portainer \
    --restart always \
    --publish 9000:9000 \
    --volume /home/core/portainer:/data \
    --volume=/var/run/docker.sock:/var/run/docker.sock \
	portainer/portainer
fi

if [ "$(docker -H ${HOST} ps -q --filter 'name=mongo')" == "" ]; then
  docker -H ${HOST} run -d \
    --name mongo \
    --restart always \
    --volume /home/core/mongodb:/data/db \
        mongo
fi

if [ -n "${PULL}" ]; then
  docker -H ${HOST} stop swarmstats
  docker -H ${HOST} rm swarmstats
fi
if [ "$(docker -H ${HOST} ps -q --filter 'name=swarmstats')" == "" ]; then
  docker -H ${HOST} pull browndog/swarmstats
  docker -H ${HOST} run -d \
    --name swarmstats \
    --restart always \
    --publish 9999:9999 \
    --volume /home/core/swarmstats:/data \
	browndog/swarmstats python ./server.py --swarm ${CLUSTER} --timeout 30
fi

if [ -n "${PULL}" ]; then
  docker -H ${HOST} stop swarmscale
  docker -H ${HOST} rm swarmscale
fi
if [ "$(docker -H ${HOST} ps -q --filter 'name=swarmscale')" == "" ]; then
  docker -H ${HOST} pull browndog/swarmscale
  docker -H ${HOST} run -d \
    --name swarmscale \
    --restart always \
    --publish 7777:7777 \
    --link swarmstats:swarmstats \
    --link mongo:mongo \
    --env "MONGO_URL=mongodb://mongo:27017" \
    --env "SWARM_URL=http://username:password@swarmstats:9999" \
    --env "Rabbitmq_URLS=${RABBITMQ}" \
        browndog/swarmscale
fi

if [ -n "${PULL}" ]; then
  docker -H ${HOST} stop swarmtoolcatalog
  docker -H ${HOST} rm swarmtoolcatalog
fi
if [ "$(docker -H ${HOST} ps -q --filter 'name=swarmtoolcatalog')" == "" ]; then
  docker -H ${HOST} pull browndog/swarmtoolcatalog
  docker -H ${HOST} run -d \
    --name swarmtoolcatalog \
    --restart always \
    --publish 5555:5555 \
    --env "DEPLOY_QUEUENAME=elasticity_deploy" \
    --env "MONGO_URL=mongodb://mongo:27017" \
    --env "SWARM_URL=http://username:password@swarmstats:9999" \
    --env "Rabbitmq_URLS=${RABBITMQ//15672/5672}" \
        browndog/swarmtoolcatalog
fi

