#!/bin/bash

WHAT=${1:-" dev-"}
TAG=${2:-"latest"}

#DEBUG=echo

HOST="192.168.5.54"

PATH=/home/kooper/bin:$PATH

for s in $(docker -H ${HOST}:2375 service ls | awk "/${WHAT}/ { print \$2 }" | sort ); do
  IMAGE=$( docker -H ${HOST}:2375 service inspect -f '{{ .Spec.TaskTemplate.ContainerSpec.Image }}' $s | sed 's/@sha256:.*//' | sed "s/:.*$/:${TAG}/" )
  ${DEBUG} docker -H ${HOST}:2375 service update --force --image ${IMAGE} ${s}
done
