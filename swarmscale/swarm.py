import logging
import requests
import bdswarm
import json

from urllib.parse import urlparse


class Swarm:

    def __init__(self, swarm_url):
        self.__logger = logging.getLogger("swarmscale")
        self.__swarm_url = swarm_url
        parsed = urlparse(swarm_url)
        self.__auth = (parsed.username, parsed.password)
        self.__mapping = {}

    def services_snapshot(self):
        url = self.__swarm_url + '/api/services?full=1'
        r = requests.get(url, auth=self.__auth)
        r.raise_for_status()
        all_services_metadata = r.json()
        self.__mapping = {}
        self.__parse_info(all_services_metadata)

    def __parse_info(self, all_services_metadata):
        for service_id in all_services_metadata:
            service_metadata = all_services_metadata[service_id]
            try:
                service_vhost = service_metadata['labels']['bd.rabbitmq.vhost']
                for swarm_prefix, vhost in bdswarm.BDSwarms.swarmprefix_vhost_mapping.items():
                    if vhost == service_vhost:
                        service_queuename = service_metadata['labels']['bd.rabbitmq.queue']
                        key = service_queuename.replace('-', '')
                        key = key.replace('.', '')
                        one_tuple = (key, service_metadata)
                        tuples = self.__mapping.setdefault(vhost, [])
                        tuples.append(one_tuple)
            except Exception as err:
                self.__logger.debug('fail to parse: ' + json.dumps(service_metadata))
                self.__logger.exception(err)

    def get_swarm_services_metadata(self, vhost):
        """
        :param vhost: rabbitmq vhost
        :return: swarm services metadata for rabbitmq vhost
        """
        return self.__mapping.get(vhost)

    def get_service_info(self, instance_name):
        url = self.__swarm_url + '/api/services/' + instance_name
        r = requests.get(url, auth=self.__auth)
        r.raise_for_status()
        return r.json()

    def scale_replica(self, instance_name, new_replica):
        url = self.__swarm_url + '/api/services/' + instance_name + '/scale/' + str(new_replica)
        r = requests.put(url, auth=self.__auth)
        r.raise_for_status()
        return self.get_service_info(instance_name)
