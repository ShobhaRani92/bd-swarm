import logging
import requests
from urllib.parse import urlparse


class Swarm:

    def __init__(self, swarm_url):
        self.__logger = logging.getLogger("swarmtoolcatalog")
        self.__swarm_url = swarm_url
        parsed = urlparse(swarm_url)
        self.__auth = (parsed.username, parsed.password)
        self.__mapping = {}

    def get_service_info(self, instance_name):
        url = self.__swarm_url + '/api/services/' + instance_name
        r = requests.get(url, auth=self.__auth)
        r.raise_for_status()
        return r.json()

    def create_service(self, swarm_servicename, min_num_instances, max_num_instances):
        self.__logger.info("invoke swarm api to create service servicename: %s, min: %d, max: %d " %
                           (swarm_servicename, min_num_instances, max_num_instances))

    def drop_service(self, swarm_servicename):
        self.__logger.info("invoke swarm api to drop service servicename: %s" % swarm_servicename)
