import logging
import pymongo


class DB:
    DB_NAME = 'bdswarm-toolcatalog'
    TBL_NAME = 'history'
    FILTER_ID = {'_id': 0}
    DESCENDING_SORT = [('_id', -1)]

    def __init__(self, database_name, tablename, url):
        client = pymongo.MongoClient(url)
        self.db = client[database_name]
        self.db.collection = self.db[tablename]
        self.__logger = logging.getLogger("swarmtoolcatalog")

    def insert_one(self, data_tuple):
        try:
            self.db.collection.insert_one(data_tuple)
        except pymongo.errors.ServerSelectionTimeoutError as err:
            self.__logger.error(err)

    def find_latest(self, n):
        ret = []
        try:
            for data_tuple in self.db.collection.find({}, {'_id': 0}).sort('timestamp', pymongo.DESCENDING).limit(n):
                ret.append(data_tuple)
        except pymongo.errors.ServerSelectionTimeoutError as err:
            self.__logger.error(err)
        return ret

    def find(self):
        """
        :return: list of metadata
        """
        ret = []
        try:
            for data_tuple in self.db.collection.find({}, {'_id': 0}):
                ret.append(data_tuple)
        except pymongo.errors.ServerSelectionTimeoutError as err:
            self.__logger.error(err)
        return ret

    def find_swarm_service_name(self, extractor_name):
        ret = []
        try:
            for data_tuple in self.db.collection.find({'extractor_name': extractor_name}, {'_id': 0}):
                ret.append(data_tuple)
        except pymongo.errors.ServerSelectionTimeoutError as err:
            self.__logger.error(err)
        return ret

    def find_one(self, query):
        """
        :param query
        :return: type of dict
        """
        try:
            if query is None:
                return self.db.collection.find_one()
            else:
                return self.db.collection.find_one(query)
        except pymongo.errors.ServerSelectionTimeoutError as err:
            self.__logger.error(err)

    def query(self, query_dic):
        """
        :param query_dic:
        :return:
        """
        ret = []
        recorders = 0
        try:
            recorders = int(query_dic.pop('last', 0))
            if recorders < 0:
                raise ValueError('non positive integer')

            for data_tuple in self.db.collection.find(query_dic, DB.FILTER_ID).limit(recorders).\
                    sort(DB.DESCENDING_SORT):
                ret.append(data_tuple)
        except Exception as err:
            self.__logger.error(err)
            return []
        return ret
