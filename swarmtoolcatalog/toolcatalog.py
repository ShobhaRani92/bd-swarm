import ast
import pika
import logging
import threading
import datetime
import dateutil.parser
import dateutil.tz
from urllib.parse import urlparse

from swarm import Swarm
from db import DB


class Listener(threading.Thread):

    def __init__(self, swarm_prefix, rabbitmq_url, mongo_url, deploy_queue_name, swarm_url, *args, **kwargs):
        super(Listener, self).__init__(*args, **kwargs)
        self.__logger = logging.getLogger("swarmtoolcatalog")
        self.__swarm_prefix = swarm_prefix
        self.__rabbitmq_url = rabbitmq_url
        self.__deploy_queue_name = deploy_queue_name
        self.__swarm = Swarm(swarm_url)

        parsed = urlparse(rabbitmq_url)
        self.vhost = parsed.path.replace("/", "")
        self.db = DB(DB.DB_NAME, DB.TBL_NAME, mongo_url)

    def run(self):
        parameters = pika.URLParameters(self.__rabbitmq_url)
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue=self.__deploy_queue_name)
        channel.basic_qos(prefetch_count=1)
        channel.basic_consume(self.__on_request, queue=self.__deploy_queue_name)
        self.__logger.info(" [x] Starting RabbitMQ connection on %s " % self.vhost)
        self.__logger.info(" [x] Waiting for tool deployment requests for %s " % self.vhost)
        try:
            channel.start_consuming()
        except KeyboardInterrupt:
            channel.stop_consuming()
        connection.close()

    def __on_request(self, ch, method, header, body):
        self.__logger.info(" [.] Received a deploy request: %s, reply_to: %s" % (body, header.reply_to))
        request_data = ast.literal_eval(body.decode("utf-8"))

        action = request_data['action']
        name = request_data['extractor_name']
        swarm_service_name = self.__swarm_prefix + '-' + name.replace('.', '_')
        self.__logger.debug(action + ' : ' + swarm_service_name)

        status = 'dropped'
        # make request to bd-swarm
        try:
            self.__swarm.get_service_info(swarm_service_name)
        except Exception as ex:
            if action == 'add':
                self.__logger.debug('ADD nono-existence' + name + ' .Then go to ADD new service for ' +
                                    swarm_service_name)
                status = 'added'
            else:
                status = 'rejected'
                if action == 'stop':
                    self.__logger.debug('DROP nono-existence' + name + ' .Then ignore')
                else:
                    self.__logger.debug(ex)

        try:
            if status == 'added':
                max_num_instances = int(request_data['max_num_instances'])
                min_num_instances = int(request_data['min_num_instances'])
                if min_num_instances <= max_num_instances:
                    self.__swarm.create_service(swarm_service_name, min_num_instances, max_num_instances)
                else:
                    status = 'rejected'
            elif status == 'dropped' and action == 'stop':
                self.__swarm.drop_service(swarm_service_name)
            else:
                status = 'rejected'
        except Exception as ex:
            status = 'rejected'
            self.__logger.debug(ex)

        # save info to database, request_data with status, timstamp
        request_data['vhost'] = self.vhost
        request_data['status'] = status
        request_data['service_name'] = swarm_service_name
        request_data['timestamp'] = datetime.datetime.utcnow().replace(tzinfo=dateutil.tz.tzutc())
        self.db.insert_one(request_data)

        self.__logger.info(" [x] Status: %s" % status)
        # reply to toolcatalog server
        try:
            ch.basic_publish(exchange="",
                             # routing_key="elasticity_deploy_status",
                             routing_key=header.reply_to,
                             properties=pika.BasicProperties(correlation_id=header.correlation_id),
                             body=str(status))
            ch.basic_ack(delivery_tag=method.delivery_tag)
        except Exception as ex:
            self.__logger.debug(ex)


class ToolCatalog:

    def __init__(self, app, rabbitmq_urls, swarm_url, mongo_url, deploy_queue_name):
        self.__logger = logging.getLogger("swarmtoolcatalog")
        self.__rabbitmqs = {}
        self.listeners = []
        self.swarm_url = swarm_url
        for swarm_prefix, rabbitmqurl in rabbitmq_urls.items():
            listener = Listener(swarm_prefix, rabbitmqurl, mongo_url, deploy_queue_name, swarm_url)
            self.listeners.append(listener)

    def start(self):
        for listener in self.listeners:
            listener.start()

