import ast
import json
import argparse
import sys
import os.path
import flask
import logging
from dateutil import tz
from datetime import datetime
from db import DB
import toolcatalog
from api_module import api_bp

app = flask.Flask(__name__)
app.config['JSON_SORT_KEYS'] = False

software_version = '1.0'
columns = ['Action', 'SwarmServiceName', 'QueueName', 'TimeStamp', 'DockerImage', 'MinInstances', 'MaxInstance',
           'Status']
toolcatalog_instance = None
db = None


@app.route('/')
def index():
    return flask.render_template('index.html', columns=columns)


def output_result(result_data):
    output = {}
    data_rows = []
    for row in result_data:
        data_row = []
        for i in range(len(columns)):
            data_row.append(str(row[columns[i]]).replace('"', '\\"'))
        data_rows.append(data_row)
    output['data'] = data_rows
    return output


def get_view_data(view_db):
    view_data = view_db.find()
    collection = []
    for datum in view_data:
        action = datum['action']
        swarm_service_name = datum['service_name']
        extractor_name = datum['extractor_name']
        timestamp = datetime.strptime(datum['timestamp'].strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')
        if 'docker_image_name' in datum:
            dockerimage = datum['docker_image_name']
        else:
            dockerimage = '--'
        if 'min_num_instances' in datum:
            mininstances = datum['min_num_instances']
        else:
            mininstances = '--'
        if 'max_num_instances' in datum:
            maxinstances = datum['max_num_instances']
        else:
            maxinstances = '--'
        status = datum['status']
        column_datum = [action, swarm_service_name, extractor_name, timestamp, dockerimage, mininstances,
                        maxinstances, status]
        collection.append(dict(zip(columns, column_datum)))
    return collection


@app.route('/list')
def list_datatable():
    collection = []
    view_data = get_view_data(db)
    if 0 != len(view_data):
        collection.extend(view_data)
    results = output_result(collection)
    # return the results as a string for the datatable
    return json.dumps(results)

if __name__ == '__main__':
    # parse command line arguments
    parser = argparse.ArgumentParser(description='Swarm Scaling Parameters')
    parser.add_argument('--logging', '-l', default=os.getenv("LOGGER", None),
                        help='logging coonfiguration (default=None)')
    parser.add_argument('--swarm_url', '-s', default=os.getenv("SWARM_URL", None),
                        help='swarm url with username :passwd')
    parser.add_argument('--rabbitmq_urls', '-r', default=os.getenv("Rabbitmq_URLS", None),
                        help='rabbitmq urls as json string')
    parser.add_argument('--mongo_url', '-m', default=os.getenv("MONGO_URL", None),
                        help='mongo url string')
    parser.add_argument('--deploy_queuename', '-d', default=os.getenv("DEPLOY_QUEUENAME", None),
                        help='deploy queue name string')

    parser.add_argument('--version', action='version', version='%(prog)s version=' + software_version)
    args = parser.parse_args()

    # setup logging
    logger = logging.getLogger(__name__)

    if not args.logging:
        logging.basicConfig(format='%(asctime)-15s %(levelname)-7s [%(threadName)-10s] : %(name)s - %(message)s',
                            level=logging.INFO)
        logging.getLogger(__name__).setLevel(logging.DEBUG)
    else:
        logging.basicConfig(format='%(asctime)-15s %(levelname)-7s [%(threadName)-10s] : %(name)s - %(message)s',
                            level=logging.DEBUG)

    if not args.swarm_url:
        logger.error("input of swarm_url empty")
        parser.print_help()
        sys.exit(-1)

    if not args.rabbitmq_urls:
        logger.error("input of rabbitmqs syntax is wrong")
        parser.print_help()
        sys.exit(-1)

    mongo_url = "mongodb://127.0.0.1:27017"
    if args.mongo_url:
        mongo_url = args.mongo_url

    deploy_queuename = "elasticity_deploy"
    if args.deploy_queuename:
        deploy_queuename = args.deploy_queuename

    db = DB(DB.DB_NAME, DB.TBL_NAME, mongo_url)
    api_bp.setup_db(DB.DB_NAME, DB.TBL_NAME, mongo_url)
    app.register_blueprint(api_bp, url_prefix='/api')

    toolcatalog_instance = toolcatalog.ToolCatalog(app, ast.literal_eval(args.rabbitmq_urls), args.swarm_url, mongo_url,
                                                   deploy_queuename)
    toolcatalog_instance.start()
    app.run(host='0.0.0.0', port=5555, debug=False)
